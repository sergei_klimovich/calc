//
//  ContentView.swift
//  Calc
//
//  Created by Admin on 11/23/20.
//

import SwiftUI

struct ContentView: View {
    
    @State var numberOne = "0"
    @State var numberTwo = "0"
    @State var action = "" //Действия над числами
    var body: some View {
        ZStack {
            Color("resultColor") //заливка фона экрана
            VStack {
                Spacer() // Пустое пространство
                //Вверхний текс куда будет выводится результат
                Text(action == "" ? numberOne : numberTwo)
                    /* if action == "" {
                     numberOne
                     } else {
                     numberTwo
                     }
                    */
                    .foregroundColor(Color.white) //цвет шрифта
                    .padding(.trailing) // отступ текста
                    .font(.custom("SF Pro", size: 45)) //Размер шрифта
                    .frame(width: UIScreen.main.bounds.width, height: 100, alignment: .trailing) // Область размером с ширину экрана
                    .background(Color("resultColor")// цвет заданный в Assets
                    ) // фон области
                
                
                HStack(spacing: 0) {
                    // AC кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("AC")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1) //Рамка вокруг кнопки
                    
                    //+/- кнопка
                    Button(action: {
                        
                    }, label: {
                        Image(systemName: "plus.slash.minus")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35)) //Системная картинка. Название взято с приложения SF Symbols https://developer.apple.com/design/
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    // % кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("%")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    //Деление кнопка
                    Button(action: {
                        self.action = "divide"
                    }, label: {
                        Image(systemName: "divide")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("actionOrange"))
                    .border(Color.black, width: 1)
                }
                HStack(spacing: 0) {
                    // AC кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("7")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1) //Рамка вокруг кнопки
                    
                    //+/- кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("8")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    // % кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("9")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    //Деление кнопка
                    Button(action: {
                        self.action = "multiply"
                    }, label: {
                        Image(systemName: "multiply")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("actionOrange"))
                    .border(Color.black, width: 1)
                }
                HStack(spacing: 0) {
                    // AC кнопка
                    Button(action: {
                        if action == "" {
                            self.numberOne = "2"
                         } else {
                            self.numberTwo = "4"
                         }
                    }, label: {
                        Text("4")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1) //Рамка вокруг кнопки
                    
                    //+/- кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("5")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    // % кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("6")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    //Деление кнопка
                    Button(action: {
                        self.action = "minus"
                    }, label: {
                        Image(systemName: "minus")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("actionOrange"))
                    .border(Color.black, width: 1)
                }
                HStack(spacing: 0) {
                    // AC кнопка
                    Button(action: {
                        if action == "" {
                            self.numberOne = "1"
                         } else {
                            self.numberTwo = "1"
                         }
                        
                    }, label: {
                        Text("1")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1) //Рамка вокруг кнопки
                    
                    //+/- кнопка
                    Button(action: {
                        if action == "" {
                            self.numberOne.append("2")
                         } else {
                             self.numberTwo.append("2")
                         }
                    }, label: {
                        Text("2")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    // % кнопка
                    Button(action: {
                        if action == "" {
                            self.numberOne = "3"
                         } else {
                            self.numberTwo = "3"
                         }
                    }, label: {
                        Text("3")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    //Деление кнопка
                    Button(action: {
                        self.action = "plus"
                    }, label: {
                        Image(systemName: "plus")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("actionOrange"))
                    .border(Color.black, width: 1)
                }
                HStack(spacing: 0) {
                    // AC кнопка
                    Button(action: {
                        
                    }, label: {
                        Text("0")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/2 //Для отображения в половину экрана
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1) //Рамка вокруг кнопки
                    
                    //+/- кнопка
                    
                    
                    // % кнопка
                    Button(action: {
                        
                    }, label: {
                        Text(".")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("acGray"))
                    .border(Color.black, width: 1)
                    
                    //Деление кнопка
                    Button(action: {
                        switch self.action {
                        case "divide":
                            let result = Int(self.numberOne)! / Int(self.numberTwo)! //Расчет
                            self.action = "" // очистка действия
                            self.numberOne = String(result) //Вывод результата
                        case "plus":
                            let result = Int(self.numberOne)! + Int(self.numberTwo)! //Расчет
                            self.action = "" // очистка действия
                            self.numberOne = String(result) //Вывод результата
                            
                        default: break
                        }
                        
                        
                    }, label: {
                        Image(systemName: "equal")
                            .foregroundColor(Color.white)
                            .font(.custom("SF Pro", size: 35))
                    })
                    .frame(width: UIScreen.main.bounds.width/4 //Для отображения четырех кнопок
                           , height: UIScreen.main.bounds.width/4, alignment: .center)
                    .background(Color("actionOrange"))
                    .border(Color.black, width: 1)
                }
                
            }
        }.edgesIgnoringSafeArea(.all) //Игнорирование системных зон
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
